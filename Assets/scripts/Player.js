﻿#pragma strict

var velocity : float;
private var movingFor = false;
private var jumpingFor = false;
private var leftmovingFor = false;

var player : Transform;
var animator : Animator;
var force : float;
var jumpTime : float = 0.3f;
var jumpDelay : float = 0.3f;
var ground : Transform;

var jumped : boolean;
var isGrounded : boolean;

function Start () {
	animator = player.GetComponent(Animator);
}

function OnGUI() {
	movingFor = false;

	if (GUI.RepeatButton (Rect (5,Screen.height - 85,120,80), "<<")) { // Left
		Debug.Log("Pressed left button");
		movingFor = true;
		leftmovingFor = true;
	}
	
	if (GUI.RepeatButton (Rect (135,Screen.height - 85,120,80), ">>")) { // Right
		Debug.Log("Pressed right button");
		movingFor = true;
		leftmovingFor = false;
	}
	
	if (GUI.Button (Rect (Screen.width - 125,Screen.height - 85,120,80), "^^")) { // Jump
		movingFor = true;
		if (isGrounded) {
			jumpingFor = true;
			jumped = true;
		}
		Debug.Log("Pressed Jump button");
	}
}

function Update () {
	Move();
}

function Move () {
	isGrounded = Physics2D.Linecast(this.transform.position, ground.position, 1<<LayerMask.NameToLayer("Plataforma"));

	if (movingFor) {
		animator.SetFloat("run", 0.2);
		if (leftmovingFor) {
			transform.Translate(Vector2.right * velocity * Time.deltaTime);
			transform.eulerAngles = new Vector2(0,180);
		} else {
			transform.Translate(Vector2.right * velocity * Time.deltaTime);
			transform.eulerAngles = new Vector2(0,0);
		}
	} else {
		animator.SetFloat("run", 0.0);	
	} 
	
	if (jumpingFor) {
		Debug.Log("boy jumping");
		animator.SetTrigger("jump");
		rigidbody2D.AddForce(transform.up * force);
		jumpTime = jumpDelay;
		jumpingFor = false;
	}
	
	jumpTime -= Time.deltaTime;
	
	if (jumpTime <= 0 && isGrounded && jumped) {
		Debug.Log("boy touch ground");
		animator.SetTrigger("ground");
		jumped = false;
	}
}